<?php
/**
 * Created by PhpStorm.
 * User: crs
 * Date: 08.10.2019
 * Time: 13:57
 */

namespace crs_sus\MessengerTarget;


class QueueTelegramClass extends \yii\base\BaseObject implements \yii\queue\RetryableJobInterface
{
    public $data;
    public $reserveEmailNotice;
    public $isFile = false;

    public function execute($queue)
    {
        if ($this->isFile){
            if (isset($this->data['file']) && isset($this->data['category']) && isset($this->data['fileName']))
                \Yii::$app->telegramPusher->sendFile($this->data['file'], $this->data['category'], $this->data['fileName'], true);
        }else{
            if (isset($this->data['message']) && isset($this->data['category']))
                \Yii::$app->telegramPusher->sendText($this->data['message'], $this->data['category'], true);
        }

    }

    public function getTtr()
    {
        return 200;
    }

    public function canRetry($attempt, $error)
    {
        //every 30 attempts send email notice
        if ($attempt % 30 == 0){
            \Yii::$app->mailer->compose()// создаём сообщение
                ->setFrom('admin@localhost')//TODO
                ->setTo($this->reserveEmailNotice)// с каждым новым получателем
                ->setSubject('Sending Log to Telegram was fail')
                ->setTextBody('Sending Log to Telegram was fail. Error: '. (is_string($error) ? $error : json_encode($error)) )
                ->send();
        }

        return $attempt < 4*60*60 ; // repeat 4 days
    }


}